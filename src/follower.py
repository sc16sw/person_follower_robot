#!/usr/bin/env python

from __future__ import division
import cv2
import numpy as np
import rospy
import sys

from geometry_msgs.msg import Twist, Vector3, Pose, Point, Quaternion, PointStamped, PoseStamped
from sensor_msgs.msg import Image, CameraInfo, LaserScan, PointCloud2
from nav_msgs.msg import Path
from visualization_msgs.msg import Marker, MarkerArray
import sensor_msgs.point_cloud2 as pc2
from human_position_estimation.msg import Poses, Detections
from cv_bridge import CvBridge, CvBridgeError
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from control_msgs.msg import PointHeadAction, PointHeadGoal
import actionlib
from actionlib_msgs.msg import *
import tf
import math
import numpy
from tf.transformations import euler_from_quaternion
import message_filters
#from gazebo_msgs.srv import GetModelState # For Testing

class personFollower():

    def __init__(self):
        """
        Constructor
        """
        self.pub = rospy.Publisher('mobile_base_controller/cmd_vel', Twist, queue_size=10)
        self.markers_pub = rospy.Publisher('visualization_marker', Marker, queue_size=10)

        # Tell the action client that we want to spin a thread by default
        self.move_base = actionlib.SimpleActionClient("move_base", MoveBaseAction)
        self.cancel = actionlib.SimpleActionClient("move_base/cancel", MoveBaseAction)
        self.move_head = actionlib.SimpleActionClient("head_controller/point_head_action", PointHeadAction)
        self.camera_info = "/xtion/rgb/camera_info"

        # Allow up to 5 seconds each for the action servers to come up
        self.move_base.wait_for_server(rospy.Duration(5))
        self.move_head.wait_for_server(rospy.Duration(5))

        # Initialise window width for calculating angular velocity
        self.window_width = 640

		# Initialise a CvBridge for image conversion
        self.bridge = CvBridge()

        # Subscriptions and synchronization for detection and depth messages
        self.detection_sub = message_filters.Subscriber("detections", Detections, queue_size=10)
        self.depth_sub = message_filters.Subscriber("xtion/depth_registered/points", PointCloud2, queue_size=10)
        self.ats = message_filters.ApproximateTimeSynchronizer([self.detection_sub, self.depth_sub], queue_size=10, slop=0.1)
        self.ats.registerCallback(self.detection_callback)

        # Transform listener for transforming points between frames
        self.listener = tf.TransformListener()

        # Initialise follower class variables
        self.centres = None
        self.cloud = None
        self.last_point = None
        self.goal_count = 0
        self.rate = rospy.Rate(10)
        self.no_detection_count = 0
        self.desired_velocity = Twist()
        self.stop = Twist()

        ## TESTING DO NOT UNCOMMENT ##
        # self.model_pub = rospy.Publisher('model_position', PointStamped)
        # self.tiago_pub = rospy.Publisher('tiago_position', PointStamped)
        # self.person_path_pub = rospy.Publisher('person_path', Path)
        # self.robot_path_pub = rospy.Publisher('robot_path', Path)
        # self.person_path = Path()
        # self.robot_path = Path()
        # self.person_coordinates = open("coordinates/person_coordinates.txt", "a+")
        # self.robot_coordinates = open("coordinates/robot_coordinates.txt", "a+")
        # self.robot_coordinate_count = 0
        # self.person_coordinate_count = 0
        # self.markers = MarkerArray()

    def image_callback(self, data):
        """
        Converts an image from the RGB camera to OpenCV format
        and display image in a new window

        Params:
            data: Image from RGB camera
        """
		# Convert the received image into a opencv image
        try:
            cv_image=self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)

        # Convert the rgb image into a hsv image
        hsv_image = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV)

        # Show the resultant images
        cv2.namedWindow('Camera_Feed')
        cv2.imshow('Camera_Feed', cv_image)
        cv2.waitKey(3)

    def detection_callback(self, centres, cloud):
        """
        Assigns the data depth data and detection centre points to instance variables.
        If at least one person is detected then the process_detection method
        is called to continue the processing of the detection.

        Params:
            centres: detection array from person detection module
            cloud: PointCloud2 from depth camera topic
        """
        # Set centres instance variable with detection
        self.centres = centres
        # Set cloud instance variable with PointCloud
        self.cloud = cloud

        # Check if at least 1 person was detected
        if len(self.centres.array) > 0:
            print("Got Detection")
            self.no_detection_count = 0
            # Process the detection
            self.process_detection()
        # No person detected
        else:
            print ("No person detected")
            self.no_detection_count += 1
            if self.no_detection_count > 20:
                self.look_for_person()

    def get_point(self, centre_x, centre_y, cloud):
        """
        Converts a pixel representing the centre point of a detected
        person to a real world coordinate with respect to the robot base.

        Params:
            centre_x: x coordinate of the detection centre point
            centre_y: y coordinate of the detection centre point
            cloud: PointCloud2 from depth camera topic

        Returns:
            PointStamped: PointStamped object with the real world
            coordinates relative to the robot base
        """
        # Initialise PointCloud2 properties
        region_size = 2
        width = cloud.width
        height = cloud.height
        point_step = cloud.point_step
        row_step = cloud.row_step

        while True:
            # Get a region of points around the centre points
            x_centres = [centre_x]
            y_centres = [centre_y]
            for i in range(1,region_size):
                x_centres.append(centre_x-i)
                y_centres.append(centre_y-i)
            for i in range(1,region_size):
                x_centres.append(centre_x+i)
                y_centres.append(centre_y+i)

            x_array = []
            y_array = []
            z_array = []

            # Extract point in PointCloud corresponding to the detection centre
            for x,y in zip(x_centres,y_centres):
                array_pos = y*row_step + x*point_step

                x_bytes = [ord(x) for x in cloud.data[array_pos:array_pos+4]]
                y_bytes = [ord(x) for x in cloud.data[array_pos+4: array_pos+8]]
                z_bytes = [ord(x) for x in cloud.data[array_pos+8:array_pos+12]]

                byte_format=struct.pack('4B', *x_bytes)
                X = struct.unpack('f', byte_format)[0]

                byte_format=struct.pack('4B', *y_bytes)
                Y = struct.unpack('f', byte_format)[0]

                byte_format=struct.pack('4B', *z_bytes)
                Z = struct.unpack('f', byte_format)[0]

                # Filter out NaN values from PointCloud as it is not dense
                if not math.isnan(X) and not math.isnan(Y) and not math.isnan(Z):
                    x_array.append(X)
                    y_array.append(Y)
                    z_array.append(Z)

            # Check at least 3 valid points exist to calculate average
            if len(x_array) >= 3:
                break

            # Increase region of points around the centre points
            region_size += 5

        x_mean = numpy.mean(x_array)
        y_mean = numpy.mean(y_array)
        z_mean = numpy.mean(z_array)

        self.listener.waitForTransform("/xtion_rgb_optical_frame", "/base_footprint", rospy.Time(0), rospy.Duration(4.0))
        depth_point = PointStamped()
        depth_point.header.frame_id="/xtion_rgb_optical_frame"
        depth_point.header.stamp=cloud.header.stamp
        depth_point.point.x=x_mean
        depth_point.point.y=y_mean
        depth_point.point.z=z_mean
        try:
            goal=self.listener.transformPoint("base_footprint", depth_point)
            return goal
        except:
            pass

    def move_forward(self, goal_point):
        """
        Moves the robot in the direction of the detected person

        Params:
            goal_point: PointStamped object containing the coordinates of the detected person
            with respect to the robot base
        """
        # Only move every 2 goals to allow time for Move Base Goal to execute
        self.goal_count += 1
        if self.goal_count == 2:
            self.goal_count = 0
            # Create a goal with respect to the base
            goal = MoveBaseGoal()
            goal.target_pose.header.frame_id = 'base_link'
            goal.target_pose.header.stamp = rospy.Time.now()
            goal.target_pose.pose.position.x = 0.5  # 0.5 meters
            goal.target_pose.pose.position.y = goal_point.point.y/(goal_point.point.x**2+goal_point.point.y**2)
            theta = math.atan2(goal_point.point.y-goal.target_pose.pose.position.y,goal_point.point.x-goal.target_pose.pose.position.x)
            quat = {'r1': 0.000, 'r2': 0.000, 'r3': np.sin(theta / 2.0), 'r4': np.cos(theta / 2.0)}
            goal.target_pose.pose.orientation = Quaternion(quat['r1'], quat['r2'], quat['r3'], quat['r4'])
            self.move_base.send_goal(goal)

    def get_camera_info(self):
        """
        Returns the camera calibration information

        Returns:
            Array containing the intrinsic camera information
        """
        msg = rospy.wait_for_message(self.camera_info, CameraInfo)
        camera_intrinsics = np.zeros((3,3), dtype=np.float64)
        camera_intrinsics[0,0] = msg.K[0]
        camera_intrinsics[1,1] = msg.K[4]
        camera_intrinsics[0,2] = msg.K[2]
        camera_intrinsics[1,2] = msg.K[5]
        camera_intrinsics[2,2] = 1
        return(camera_intrinsics)

    def publish_marker(self, x, y, z, colour):
        """
        Publishes a visualization marker for visualizing detection
        locations in Rviz

        Params:
            param x: x coordinate of person location
            param y: y coordinate of person location
            param z: z coordinate of person location
            param colour: colour of marker - "red" or "blue"
        """
        # Create Marker and add meta-information
        marker = Marker()
        marker.header.frame_id = "/map"
        marker.header.stamp = rospy.Time.now()
        marker.ns = "sjw"
        marker.type = marker.SPHERE
        marker.action = marker.ADD

        # Marker pose
        marker.pose.position.x = x
        marker.pose.position.y = y
        marker.pose.position.z = z

        # Marker orientation
        marker.pose.orientation.x = 0.0;
        marker.pose.orientation.y = 0.0;
        marker.pose.orientation.z = 0.0;
        marker.pose.orientation.w = 1.0

        # Marker scale
        marker.scale.x = 0.1
        marker.scale.y = 0.1
        marker.scale.z = 0.1

        # Marker colour
        if colour == "blue":
            marker.color.a = 1.0
            marker.color.r = 0.0
            marker.color.g = 0.0
            marker.color.b = 1.0

        else:
            marker.color.a = 1.0
            marker.color.r = 1.0
            marker.color.g = 0.0
            marker.color.b = 1.0

        # Marker lifetime
        marker.lifetime.secs = 1

        # Publish the marker for use in Rviz
        self.markers_pub.publish(marker)

    def process_detection(self):
        """
        Processes detections, moves the head and base as long as the person
        is 1.5m away or more. Also publishes visualization markers for use
        with Rviz.
        """

        # If no people are detected then cancel all goals
        if not len(self.centres.array) > 0:
            print("No person detected")
            self.move_base.cancel_all_goals()
        # If more than one person is detected then cancel all goals
        elif len(self.centres.array) > 1:
            print("More than one person detected")
            self.move_base.cancel_all_goals()
            self.move_head.cancel_all_goals()
        else:
            # Get the location of the person with respect to the robot base
            base_goal = self.get_point(self.centres.array[0].centre_x, self.centres.array[0].centre_y, self.cloud)

            # Transform the goal point into the map frame
            self.listener.waitForTransform("/base_footprint", "/map", rospy.Time(0), rospy.Duration(4.0))
            try:
                map_goal=self.listener.transformPoint("map", base_goal)
                self.publish_marker(map_goal.point.x, map_goal.point.y, map_goal.point.z, "blue")
            except:
                print("Could not transform point from base frame to map frame")
                return

            # Check if a detection has previously been made
            if self.last_point != None:
                # time = self.centres.header.stamp.secs-self.last_point.header.stamp.secs
                # if time == 0:
                #     time = 1
                # # Distance between last known point of person and current point
                distance = abs ( math.sqrt(base_goal.point.x**2+base_goal.point.y**2) - math.sqrt(self.last_point.point.x**2+self.last_point.point.y**2) )
                # velocity = distance/time
                # Only accept as valid point if distance is less than 1.5m (Speed of 1.5m/s)
                if distance >= 1.5:
                    print("Person moving greater than 1.5m/s")
                    self.publish_marker(map_goal.point.x, map_goal.point.y, map_goal.point.z, "red")
                    return
                else:
                    # Update the last known detection point to be the current detection point
                    self.last_point = base_goal
                    # Publish the marker for use with Rviz
                    self.publish_marker(map_goal.point.x, map_goal.point.y, map_goal.point.z, "blue")
            # This is the first detection, so accept the point as valid
            else:
                self.last_point = base_goal
                self.publish_marker(map_goal.point.x, map_goal.point.y, map_goal.point.z, "blue")

            # Only move forward if distance to person is at least 2m
            if math.sqrt(base_goal.point.x**2+base_goal.point.y**2) >= 2:
                print("Moving forward...")
                # Turn the head to look at the person
                self.look_to_point_in_map(map_goal)
                # Move towards the person
                self.move_forward(base_goal)
            # Person is detected, but depth point missing from PointCloud
            elif math.isnan(base_goal.point.x):
                print("Trying to get detection location...")
            # Person is within 2m so cancel all goals
            else:
                print("Reached within 2 metres of target person")
                self.move_base.cancel_all_goals()
                # Keep the person centred in the optical frame
                self.look_to_point_in_map(map_goal)

    def look_to_point_in_map(self, map_goal):
        """
        Moves the head to keep the detected person in view.

        Params:
            map_goal: PointStamped object containing the coordinates of the detected
            person with respect to the map
        """
        point_stamped = PointStamped()
        point_stamped.header.frame_id = "/map"
        point_stamped.header.stamp = rospy.Time.now()
        point_stamped.point.x = map_goal.point.x
        point_stamped.point.y = map_goal.point.y
        point_stamped.point.z = 1.21
        goal = PointHeadGoal()
        goal.pointing_frame = "/xtion_rgb_optical_frame"
        goal.pointing_axis.x = 0.0
        goal.pointing_axis.y = 0.0
        goal.pointing_axis.z = 1.0
        goal.max_velocity = 0.7
        goal.target = point_stamped
        print("Moving head")
        self.move_head.send_goal(goal)

    def look_for_person(self):
        """
        Searches for a person by rotating the base in a circular motion
        if the person has been lost.
        """
        self.last_point = None
        self.desired_velocity.angular.z = 0.5
        self.pub.publish(self.desired_velocity)
        self.rate.sleep()

    ### FOR TESTING ###
    # def publish_model_state(self):
    #     self.model_coordinates = rospy.ServiceProxy( '/gazebo/get_model_state', GetModelState)
    #     self.object_coordinates = self.model_coordinates("citizen_extras_female_02", "/map")
    #     pose = PoseStamped()
    #     pose.header.stamp = rospy.Time.now()
    #     pose.header.frame_id = "map"
    #     pose.pose = self.object_coordinates.pose
    #     self.person_path.poses.append(pose)
    #     self.person_path.header.stamp = rospy.Time.now()
    #     self.person_path.header.frame_id = "map"
    #     self.person_coordinate_count += 1
    #     if self.person_coordinate_count == 10:
    #         self.person_coordinate_count = 0
    #         self.person_coordinates.write("{}, {}\n".format (pose.pose.position.x,pose.pose.position.y))
    #
    #     self.model_coordinates = rospy.ServiceProxy( '/gazebo/get_model_state', GetModelState)
    #     self.object_coordinates = self.model_coordinates("tiago_steel", "/map")
    #     pose = PoseStamped()
    #     pose.header.stamp = rospy.Time.now()
    #     pose.header.frame_id = "map"
    #     pose.pose = self.object_coordinates.pose
    #     self.robot_path.poses.append(pose)
    #     self.robot_path.header.stamp = rospy.Time.now()
    #     self.robot_path.header.frame_id = "map"
    #     self.robot_coordinate_count += 1
    #     if self.robot_coordinate_count == 10:
    #         self.robot_coordinate_count = 0
    #         self.robot_coordinates.write("{}, {}\n".format (pose.pose.position.x,pose.pose.position.y))
    #
    #     self.person_path_pub.publish(self.person_path)
    #     self.robot_path_pub.publish(self.robot_path)

def main(args):
	# Instantiate the personFollower class
	# And rospy.init the entire node
    rospy.init_node('personFollower', anonymous=True)
    pF = personFollower()
	# Ensure that the node continues running with rospy.spin()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        pF.move_base.cancel_all_goals()
        print("Shutting Down")

	# Destroy all image windows before closing node
    cv2.destroyAllWindows()
    pF.move_base.cancel_all_goals()

# Check if the node is executing in the main path
if __name__ == '__main__':
	main(sys.argv)
