#!/usr/bin/env python

"""
    Human Detection Service.

    ROS service that takes as input
    an image in a MAT format, and
    returns a frame with detections
    and their respective centre points.
"""

# Modules
import os
import cv2
import sys
import time
import math
import rospy
import imutils
import numpy as np
import message_filters

# OpenCV-ROS bridge
from cv_bridge import CvBridge, CvBridgeError

# Messages for requests and subscriptions
from sensor_msgs.msg import Image, PointCloud2
from human_position_estimation.srv import *

from pathlib import Path
from sensor_msgs.msg import Image
from human_position_estimation.msg import *
# from human_position_estimation.srv import * # SAM commented out

class PersonDetection:

    def __init__(self):
        """
            Constructor.
        """
        # Detection target ID (person)
        self.target = 15

        # Minimum confidence for acceptable detections
        self.confidence = 0.5

        # Converted depth_image
        self.depth_image = None

        # Publishing rate
        self.rate = rospy.Rate(10)

        # Number of detections
        self.number_of_detections = 0

        # Detection messages
        self.detections = Detections()

        # Constant path
        self.path = str(Path(os.path.dirname(os.path.abspath(__file__))).parents[0])

        # Define detection's target/s
        self.targets = ["background", "aeroplane", "bicycle", "bird", "boat",
                        "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
                        "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
                        "sofa", "train", "tvmonitor"]

        # Bounding boxes self.colours
        self.colours = np.random.uniform(0, 255, size=(len(self.targets), 3))

        # Load the neural network serialised model
        self.net = cv2.dnn.readNetFromCaffe(self.path + "/data/nn_params/MobileNetSSD_deploy.prototxt.txt",
                                            self.path + "/data/nn_params/MobileNetSSD_deploy.caffemodel")

        # Distance (human-robot distance) and detection publishers
        self.detection_pub = rospy.Publisher('detections', Detections, queue_size=1)

        print("[INFO] Successful DNN Initialisation")

        # Subscriptions (via Subscriber package)
        rgb_sub = message_filters.Subscriber("/xtion/rgb/image_rect_color", Image, queue_size=None) ## sam added queue size = None ##
        # depth_sub = message_filters.Subscriber("/xtion/depth_registered/hw_registered/image_rect_raw", Image)
        depth_sub = message_filters.Subscriber("/xtion/depth_registered/image_raw", Image, queue_size=None) ## sam added queue size = None ##

        # Synchronize subscriptions
        ats = message_filters.ApproximateTimeSynchronizer([rgb_sub, depth_sub], queue_size=1, slop=0.1) ## sam changed queue size from 5 and slop from 0.5
        ats.registerCallback(self.processSubscriptions)

    def detection(self, req):
        """
            Returns the frame with
            detections bounding boxes.

            Params:
                sensor_msgs/Image: Depth image syncd with RGB

            Ouput:
                int: Result of the service
        """
        print("[INFO] Loading Image...")
        frame = self.load_img()

        # Resize image to be maximum 400px wide
        frame = imutils.resize(frame, width = 400)

        # Blob conversion (detecion purposes)
        (h, w) = frame.shape[:2]
        blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 0.007843, (300, 300), 127.5)

        # Run feed-forward (crates detection array)
        print("[INFO] Detection...")
        self.net.setInput(blob)
        detections = self.net.forward()

        # Loop over the detections
        for i in np.arange(0, detections.shape[2]):
            # Get detection probability
            confidence = detections[0, 0, i, 2]

            # Get ID of the detection object
            idx = int(detections[0, 0, i, 1])

            # Filter out non-human detection with low confidence
            if confidence > self.confidence and idx == self.target:
                # `detections`, then compute the (x, y)-coordinates of
                # the bounding box for the object
                box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                (startX, startY, endX, endY) = box.astype("int")

                # Rectangle keypoints
                top_left = (startX, startY)
                bottom_right = (endX, endY)

                # draw bounding box
                label = "{}: {:.2f}%".format(self.targets[idx], confidence * 100)
                roi = cv2.rectangle(frame, top_left, bottom_right, self.colours[idx], 2)
                y = startY - 15 if startY - 15 > 15 else startY + 15
                cv2.putText(frame, label, (startX, y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, self.colours[idx], 2)

                # Get centre point of the rectangle and draw it
                centre_point = self.getCentre(top_left, bottom_right)
                cv2.circle(frame, centre_point, 4, (0,0,255), -1)

                # Get 480x640 ratio points
                centre_ratio_point = self.getRatioPoint(centre_point[0], centre_point[1])

                # Detection info
                detection = Detection()
                detection.ID = self.number_of_detections
                detection.centre_x = centre_ratio_point[0]
                detection.centre_y = centre_ratio_point[1]

                # Aggregate the detection to the others
                self.detections.array.append(detection)

                # Human detections counter
                self.number_of_detections += 1

        # Save frame
        self.store(frame)

        # Add number_of_detections item to the detections message
        self.detections.number_of_detections = self.number_of_detections

        # Request depth mapping for the detections
        # rospy.loginfo("Requesting depth mapping for the detections...")
        # self.requestMapping(self.detections, req.depth)
        #
        # return RequestDetectionResponse("success")
        self.requestMapping(self.detections)
        # return("Success")

    def requestMapping(self, detections):
        """
            ROS service that requests the
            depth distance of the detections
            from the RGBD-optical frame.

            Arguments:
                detections: RGB detections rgb position
        """
        # Wait for service to come alive
        # rospy.wait_for_service('detection_pose')

        try:
            # Build request
            # request = rospy.ServiceProxy('detection_pose', RequestDepth)
            #
            # # Get response from service
            # response = request(detections, depth_image)

            print("Publishing detections")

            # Publish detections
            self.detections.header.stamp = rospy.Time.now() ## ADDED BY SAM ##
            self.detection_pub.publish(self.detections)

            # Access the response field of the custom msg
            # rospy.loginfo("Pose service: %s", response.res)

            # Clean
            self.detections = Detections()
            self.number_of_detections = 0

        except rospy.ServiceException as e:
            rospy.loginfo("Depth service call failed: %s", e)

    def getDetectionObject(self, confidence, rgb_x, rgb_y):
        """
            Detection object for
            detection custom message
            population.

            Arguments:
                param1: detection confidence
                param2: x coordinate of the centre box
                param3: y coordinate of the centre box

            Returns:
                object: detection object
        """
        return {'confidence': confidence, 'rgb_x': rgb_x, 'rgb_y': rgb_y}

    # Load image to be processed
    def load_img(self):
        """
            Load image to be processed.

            Returns:
                image: MAT image
        """
        return cv2.imread(self.path + "/data/converted/image.png")

    def store(self, frame):
        """
            Stores image with
            detections' bounding
            boxes.

            Arguments:
                param1: MAT image with detection boxes
        """
        cv2.imwrite(self.path + "/data/detections/detections.png", frame)

    def getCentre(self, tl, br):
        """
            Finds centre point of the
            bounding box with respect
            to the 300x300 ratio.

            Arguments:
                int: Top left corner of the rectangle
                int: Bottom right corner of the rectangle

            Returns:
                tuple of ints: X and Y coordinate of centre point
        """
        # Compute distances
        width  = br[0] - tl[0]
        height = br[1] - tl[1]

        # Return centre
        return (tl[0] + int(width * 0.5), tl[1] + int(height * 0.5))

    def getRatioPoint(self, x, y):
        """
            Find point in the
            480x640 ratio.

            Arguments:
                int: 400x300 X coordinate (width)
                int: 400x300 Y coordinate (height)

            Returns:
                tuple of ints: X and Y coordinate of centre point
        """
        # return (int((x/400) * 640), int((y/300) * 480))
        ## sam ##
        return (int((x*640) / 400), int((y*480) / 300))








    # Constant path
    PATH = str(Path(os.path.dirname(os.path.abspath(__file__))).parents[0])

    # def requestDetection(req):
    #     """
    #         Sends a service request to
    #         the person detection module.
    #
    #         Arguments:
    #             sensor_msgs/Image: Depth image
    #
    #         Returns:
    #             string: Service response
    #     """
    #     # Wait for service to come alive
    #     rospy.wait_for_service('detection')
    #
    #     try:
    #         # Build request
    #         request = rospy.ServiceProxy('detection', RequestDetection)
    #
    #         # Get response from service
    #         response = request(req)
    #
    #         # Access the response field of the custom msg
    #         rospy.loginfo("Detection service: %s", response.res)
    #         # return response.res
    #
    #     except rospy.ServiceException as e:
    #         rospy.loginfo("Detection service call failed: %s", e)

    def store(self, cv_image):
        """
            Stores the converted raw image from
            the subscription and writes it to
            memory.

            Arguments:
                MAT: OpenCV formatted image
        """
        cv2.imwrite(self.PATH + "/data/converted/image.png", cv_image)

    def toMAT(self, rgb_image):
        """
            Converts raw RGB image
            into OpenCV's MAT format.

            Arguments:
                sensor_msgs/Image: RGB raw image

            Returns:
                MAT: OpenCV BGR8 MAT format
        """
        try:
            cv_image = CvBridge().imgmsg_to_cv2(rgb_image, 'bgr8')
            return cv_image

        except Exception as CvBridgeError:
            print('Error during image conversion: ', CvBridgeError)

    def processSubscriptions(self, rgb_image, depth_image):
        """
            Callback for the TimeSynchronizer
            that receives both the rgb raw image
            and the depth image, respectively
            running the detection module on the
            former and the mappin process on the
            former at a later stage in the chain.

            Arguments:
                sensor_msgs/Image: The RGB raw image
                sensor_msgs/Image: The depth image
        """
        print("Got depth and rgb.")
        # Processing the rgb image
        rgb_cv_image = self.toMAT(rgb_image)
        self.store(rgb_cv_image)

        # Request services
        # rospy.loginfo("Requesting detection and mapping services")
        # requestDetection(depth_image)
        self.detection(depth_image)

def main(args):

    # Initialise node
    rospy.init_node('person_detection', anonymous=True)

    try:
        # Initialise
        hd = PersonDetection()

        # Detection service
        # service = rospy.Service('detection', RequestDetection, hd.detection)

        # Spin it baby !
        rospy.spin()

    except KeyboardInterrupt as e:
        print('Error during main execution' + e)

# Execute main
if __name__ == '__main__':
    main(sys.argv)
